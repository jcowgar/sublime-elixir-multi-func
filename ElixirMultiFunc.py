import sublime
import sublime_plugin

bottom_arrow = "Packages/GitGutter/themes/Default/deleted_bottom_arrow@2x.png"
dual_arrow = "Packages/GitGutter/themes/Default/deleted_dual_arrow@2x.png"
top_arrow = "Packages/GitGutter/themes/Default/deleted_top_arrow@2x.png"


class ElixirMultiFuncEventListener(sublime_plugin.ViewEventListener):
    def on_modified_async(self):
        self.view.erase_regions("elixir-func-head-first")
        self.view.erase_regions("elixir-func-head-middle")
        self.view.erase_regions("elixir-func-head-last")

        symbols = self.view.indexed_symbol_regions(
            type=sublime.SymbolType.DEFINITION)

        last = symbols[0]
        in_run = False

        first_regions = []
        middle_regions = []
        last_regions = []

        for s in symbols[1:]:
            if in_run is True and last.name != s.name:
                last_regions.append(last.region)
                in_run = False
            elif in_run is False and last.name == s.name:
                first_regions.append(last.region)
                in_run = True
            elif in_run:
                middle_regions.append(last.region)
            last = s

        if in_run is True:
            last_regions.append(last.region)

        self.view.add_regions("elixir-func-head-first", first_regions,
                              "region.greenish", bottom_arrow,
                              sublime.HIDDEN | sublime.PERSISTENT,
                              ["&#x2AF1;" for x in first_regions])
        self.view.add_regions("elixir-func-head-middle", middle_regions,
                              "region.yellowish", dual_arrow,
                              sublime.HIDDEN | sublime.PERSISTENT,
                              ["&#x29F2;" for x in middle_regions])
        self.view.add_regions("elixir-func-head-last", last_regions,
                              "region.redish", top_arrow,
                              sublime.HIDDEN | sublime.PERSISTENT,
                              ["&#x27DF;" for x in last_regions])
